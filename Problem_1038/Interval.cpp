#include "Interval.h"

Interval60::Interval60(int v) {
  if (v < 0) {
    throw OutOfBoundaries();
    v = 0;
  }

  end = (start = v * 60) + 59;
}

// Interval60::Interval60(int s, int e) : start(((s >= 0) && (s <= 59)) ? s : 0), end(((e >= 0) && (e <= 59)) ? e : 59) { }
Interval60::Interval60(int t, int v) {
  if (v < 0) {
    throw OutOfBoundaries();
    v = 0;
  }

  end = (start = v * 60 - t) + 59;
}

Interval60::~Interval60() { }

void Interval60::Shift(int t) {
  start -= t;
  end -= t;
}

bool Interval60::Intersect(const Interval60 &i) {
  /*
    if ((start >= i.start) && (start <= i.end)) {
      end = (end > i.end) ? i.end : end;
      return true;
    }
    else if ((start < i.start) && (end >= i.start)) {
      start = i.start;
      end = (end > i.end) ? i.end : end;
      return true;
    }
    else {
      return false;
    }
  */
  if ((start >= i.start && start <= i.end) || (i.start >= start && i.start <= end)) {
    start = (start > i.start) ? start : i.start;
    end = (end < i.end) ? end : i.end;
    return true;
  }
  else {
    return false;
  }
}

int Interval60::GetStart() const {
  return start;
}

int Interval60::GetEnd() const {
  return end;
}
