#pragma once

class Exception { };
class OutOfBoundaries : public Exception { };

class Interval60
{
	int start;
	int end;

public:
	Interval60(int);
	Interval60(int, int);
	~Interval60();

	void Shift(int);
	bool Intersect(const Interval60 &);
	int GetStart() const;
	int GetEnd() const;
};
