
#define _DONE_

#include <iostream>
#include "Interval.h"

using namespace std;

int main() {
	int n;
	bool repeat, solved;

	Interval60 x(0), *iv;

	int (*points)[2];

#ifndef _DONE_
	cout << "Enter the number of snapshots:" << endl;

	do {
		repeat = false;
#endif

		cin >> n;

#ifndef _DONE_
		if ((n < 1) || (n > 10)) {

			cout << "Wrong input! Please enter new value: ";
			repeat = true;
		}
	} while (repeat);
#endif

	points = new int[n][2];

#ifndef _DONE_
	cout << "Snapshots taken at:" << endl;

	do {
		repeat = false;
		cin.seekg(0, ios::end);
		cin.clear();
#endif

		cin >> points[0][0];

		for (int i = 1; i < n; i++) {
			cin >> points[i][0];

#ifndef _DONE_
			if (points[i][0] <= points[i - 1][0]) {
				cout << "Wrong input! Please enter new values: ";
				repeat = true;
				break;
			}
#endif

		}

#ifndef _DONE_
} while (repeat);

	cout << "The clock readings on the snapshots: " << endl;

	do {
		repeat = false;
		cin.seekg(0, ios::end);
		cin.clear();
#endif

		cin >> points[0][1];

		for (int i = 1; i < n; i++) {

			cin >> points[i][1];

#ifndef _DONE_
			if (points[i][1] < points[i - 1][1]) {
				cout << "Wrong input! Please enter new values: ";
				repeat = true;
				break;
			}
#endif

		}

#ifndef _DONE_
	} while (repeat);
#endif

	for (int i = 0; i < n; i++) {
		iv = new Interval60(points[i][0], points[i][1]);
		solved = x.Intersect(*iv);
		delete iv;

		if (!solved) {
			break;
		}
	}

	if (solved) {
		cout << x.GetStart() << ' ' << x.GetEnd() << endl;
	}
	else {
		cout << "-1 -1" << endl;
	}

#ifndef _DONE_
	system("pause");
#endif

	return 0;
}
